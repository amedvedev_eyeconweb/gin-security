package http_security

import (
	"net/http"
)

type AuthUser struct {
	ID    int64
	Roles []string
	Login string
}

type Security interface {
	PolicyManager() Manager
	Can(r *http.Request, action string) (bool, error)
	AuthUser(r *http.Request) (AuthUser, error)
}
