package http_security_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/amedvedev_eyeconweb/http-security"
	"testing"
)

var mp http_security.Manager

func init() {
	mp = http_security.NewManager(http_security.NewMemoryTransport())

	_ = mp.AddGroup("account_reader")
	_ = mp.AddGroup("account_manager")
	_ = mp.AddGroup("account_admin")

	_ = mp.AddGroup("application_reader")
	_ = mp.AddGroup("application_manager")
	_ = mp.AddGroup("application_admin")

	_ = mp.AddGroup("root")

	_ = mp.AddAction("read_account")
	_ = mp.AddAction("create_account")
	_ = mp.AddAction("delete_account")

	_ = mp.AddAction("read_application")
	_ = mp.AddAction("create_application")
	_ = mp.AddAction("delete_application")

	_ = mp.AddGroupToAction("account_reader", "read_account")
	_ = mp.AddGroupToAction("account_manager", "create_account")
	_ = mp.AddGroupToAction("account_admin", "delete_account")

	_ = mp.AddGroupToGroup("account_manager", "account_reader")
	_ = mp.AddGroupToGroup("account_admin", "account_manager")

	_ = mp.AddGroupToAction("application_reader", "read_application")
	_ = mp.AddGroupToAction("application_manager", "create_application")
	_ = mp.AddGroupToAction("application_admin", "delete_application")

	_ = mp.AddGroupToGroup("application_manager", "application_reader")
	_ = mp.AddGroupToGroup("application_admin", "application_manager")

	_ = mp.AddGroupToGroup("root", "account_admin")

	mp.RefreshPolicy()
}

func TestGroupToGroupRecursively(t *testing.T) {
	err := mp.AddGroupToGroup("account_reader", "root")
	assert.EqualError(t, err, http_security.ErrGroupToGroupRecursively.Error())
}

func TestGroupExist(t *testing.T) {
	err := mp.AddGroup("root")
	assert.EqualError(t, err, http_security.ErrGroupExist.Error())
}

func TestActionExist(t *testing.T) {
	err := mp.AddAction("read_account")
	assert.EqualError(t, err, http_security.ErrActionExist.Error())
}

func TestGroupToActionExist(t *testing.T) {
	err := mp.AddGroupToAction("account_reader", "read_account")
	assert.EqualError(t, err, http_security.ErrGroupToActionExist.Error())
}

func TestGroupToGroupExist(t *testing.T) {
	err := mp.AddGroupToGroup("account_manager", "account_reader")
	assert.EqualError(t, err, http_security.ErrGroupToGroupExist.Error())
}

func TestBase(t *testing.T) {
	tests := []struct {
		policyName string
		result     []string
	}{
		{
			policyName: "read_account",
			result:     []string{"account_admin", "account_manager", "account_reader", "root"},
		},
		{
			policyName: "create_account",
			result:     []string{"account_admin", "account_manager", "root"},
		},
		{
			policyName: "delete_account",
			result:     []string{"account_admin", "root"},
		},
		{
			policyName: "read_application",
			result:     []string{"application_admin", "application_manager", "application_reader"},
		},
	}
	for _, test := range tests {
		t.Run(test.policyName, func(t *testing.T) {
			rule, _ := mp.PolicyName(test.policyName)
			assert.Equal(t, test.result, rule, "Policy not equal")
		})
	}
}
