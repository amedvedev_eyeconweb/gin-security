package jwt_security

import (
	"context"
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/amedvedev_eyeconweb/http-security"
	"net/http"
	"strings"
)

const InitAuthKey = "initAuth"

type AccessClaims struct {
	UUID  string   `json:"uuid"`
	ID    int64    `json:"id"`
	Roles []string `json:"roles"`
	Login string   `json:"login"`
	jwt.StandardClaims
}

type RefreshClaims struct {
	UUID string `json:"uuid"`
	jwt.StandardClaims
}

type jwtSecurity struct {
	publicKey string
	manager   http_security.Manager
}

func (js *jwtSecurity) PolicyManager() http_security.Manager {
	return js.manager
}

func (js *jwtSecurity) Can(r *http.Request, a string) (bool, error) {
	authUser, err := js.AuthUser(r)
	if err != nil {
		return false, err
	}
	p := js.PolicyManager().Policy()

	var ruleExist bool
	var rule []string
	if rule, ruleExist = p[a]; !ruleExist {
		return true, nil //not denied policy
	}

	for _, role := range authUser.Roles {
		for _, ruleRole := range rule {
			if ruleRole == role {
				return true, nil
			}
		}
	}
	return false, http_security.ErrForbidden
}

func (js *jwtSecurity) AuthUser(r *http.Request) (http_security.AuthUser, error) {
	var authUser http_security.AuthUser

	r, err := js.initAuth(r)
	if err != nil {
		return authUser, err
	}
	authUserCtx := r.Context().Value("authUser")

	if authUserCtx != nil {
		authUser = authUserCtx.(http_security.AuthUser)
		return authUser, nil
	}

	return authUser, http_security.ErrAuthNotFound
}

func (js *jwtSecurity) initAuth(r *http.Request) (*http.Request, error) {
	initAuth := r.Context().Value(InitAuthKey)

	if initAuth == nil {
		ctx := context.WithValue(r.Context(), InitAuthKey, true)
		r = r.WithContext(ctx)
	} else {
		return r, nil
	}

	var authUser http_security.AuthUser
	reqToken := r.Header.Get("Authorization")
	if len(reqToken) == 0 {
		return r, ErrAuthorizationHeaderEmpty
	}

	splitToken := strings.Split(reqToken, "Bearer ")
	if len(splitToken) != 2 {
		return r, ErrBearerTokenEmpty
	}

	at := strings.TrimSpace(splitToken[1])
	pKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(js.publicKey))
	if err != nil {
		return r, err
	}
	token, err := jwt.ParseWithClaims(at, &AccessClaims{}, func(token *jwt.Token) (interface{}, error) {
		return pKey, nil
	})
	if err != nil {
		verr, ok := err.(*jwt.ValidationError)
		if ok && verr.Errors == jwt.ValidationErrorExpired {
			return r, ErrExpiredToken
		}
		return r, ErrInvalidToken
	}
	claims, ok := token.Claims.(*AccessClaims)
	if !ok {
		return r, ErrInvalidToken
	}

	authUser = http_security.AuthUser{
		ID:    claims.ID,
		Login: claims.Login,
		Roles: claims.Roles,
	}
	ctx := context.WithValue(r.Context(), "authUser", authUser)
	r = r.WithContext(ctx)
	return r, nil
}

func NewJwtSecurity(publicKey string, pm http_security.Manager) http_security.Security {
	return &jwtSecurity{
		publicKey: publicKey,
		manager:   pm,
	}
}

func GenerateAccessToken(privateKey string, claims AccessClaims) (string, error) {
	signKey, err := signKey(privateKey)
	if err != nil {
		return "", err
	}
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return t.SignedString(signKey)
}

func GenerateRefreshToken(primaryKey string, claims RefreshClaims) (string, error) {
	signKey, err := signKey(primaryKey)
	if err != nil {
		return "", err
	}
	rt := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return rt.SignedString(signKey)
}

func signKey(key string) (*rsa.PrivateKey, error) {
	return jwt.ParseRSAPrivateKeyFromPEM([]byte(key))
}
