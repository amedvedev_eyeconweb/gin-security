package jwt_security_test

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/amedvedev_eyeconweb/http-security"
	"gitlab.com/amedvedev_eyeconweb/http-security/jwt_security"
	"net/http"
	"testing"
	"time"
)

var publicKey string
var privateKey string

func init() {
	keyUsecase := jwt_security.NewKeypairUsecase()
	keyPair, _ := keyUsecase.PairGenerate()
	publicKey = keyPair.PublicKey
	privateKey = keyPair.PrivateKey
}

func TestAuthSuccess(t *testing.T) {
	js := jwt_security.NewJwtSecurity(publicKey, http_security.NewManager(http_security.NewMemoryTransport()))
	testAuthUser := http_security.AuthUser{
		ID:    1,
		Roles: []string{"root"},
		Login: "root",
	}
	accessClaims := jwt_security.AccessClaims{
		UUID:  "xxx-xxx-xxx",
		ID:    testAuthUser.ID,
		Roles: testAuthUser.Roles,
		Login: testAuthUser.Login,
	}
	accessToken, _ := jwt_security.GenerateAccessToken(privateKey, accessClaims)
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", accessToken))
	authUser, _ := js.AuthUser(request)
	assert.Equal(t, testAuthUser, authUser)
}

func TestAuthNotFound(t *testing.T) {
	js := jwt_security.NewJwtSecurity("key", http_security.NewManager(http_security.NewMemoryTransport()))
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	ctx := context.WithValue(request.Context(), jwt_security.InitAuthKey, true)
	request = request.WithContext(ctx)
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, http_security.ErrAuthNotFound.Error())
}

func TestAuthorizationHeaderEmpty(t *testing.T) {
	js := jwt_security.NewJwtSecurity("key", http_security.NewManager(http_security.NewMemoryTransport()))
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, jwt_security.ErrAuthorizationHeaderEmpty.Error())

}

func TestBearerTokenEmpty(t *testing.T) {
	js := jwt_security.NewJwtSecurity("key", http_security.NewManager(http_security.NewMemoryTransport()))
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	request.Header.Add("Authorization", "BearerTokenEmpty")
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, jwt_security.ErrBearerTokenEmpty.Error())
}

func TestKeyMustBePEMEncoded(t *testing.T) {
	js := jwt_security.NewJwtSecurity("key", http_security.NewManager(http_security.NewMemoryTransport()))
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	request.Header.Set("Authorization", "Bearer KeyMustBePEMEncoded")
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, jwt.ErrKeyMustBePEMEncoded.Error())
}

func TestExpiredToken(t *testing.T) {
	now := time.Now()
	then := now.AddDate(0, -1, 0)
	js := jwt_security.NewJwtSecurity(publicKey, http_security.NewManager(http_security.NewMemoryTransport()))
	expiredToken, _ := jwt_security.GenerateAccessToken(privateKey, jwt_security.AccessClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: then.Unix(), //TODO сделать настраиваемым
		},
	})
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", expiredToken))
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, jwt_security.ErrExpiredToken.Error())
}

func TestInvalidToken(t *testing.T) {
	js := jwt_security.NewJwtSecurity(publicKey, http_security.NewManager(http_security.NewMemoryTransport()))
	request, _ := http.NewRequest(http.MethodGet, "/test", nil)
	request.Header.Set("Authorization", "Bearer ErrInvalidToken")
	_, err := js.AuthUser(request)
	assert.EqualError(t, err, jwt_security.ErrInvalidToken.Error())
}
