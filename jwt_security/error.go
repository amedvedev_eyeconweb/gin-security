package jwt_security

import (
	"errors"
)

var (
	// ErrAuthorizationHeaderEmpty ...
	ErrAuthorizationHeaderEmpty = errors.New("authorization header not found")
	// ErrBearerTokenEmpty ...
	ErrBearerTokenEmpty = errors.New("bearer token empty")
	// ErrExpiredToken ...
	ErrExpiredToken = errors.New("token has expired")
	// ErrInvalidToken ...
	ErrInvalidToken = errors.New("token is invalid")
)
