package http_security

type memoryTransport struct {
}

func NewMemoryTransport() Transport {
	return &memoryTransport{}
}

func (m *memoryTransport) SavePolicy() error {
	return nil
}

func (m *memoryTransport) LoadPolicy() error {
	return nil
}
