package http_security

import (
	"sort"
)

type Group struct {
	Name string
}

type GroupToGroup struct {
	Group  Group
	Parent Group
}

type Action struct {
	Name string
}

type ActionGroup struct {
	Action Action
	Group  Group
}

type Transport interface {
	SavePolicy() error
	LoadPolicy() error
}

type Manager interface {
	AddGroup(group string) error
	AddAction(action string) error
	AddGroupToAction(group string, action string) error
	AddGroupToGroup(group string, parentGroup string) error
	RefreshPolicy()
	SavePolicy() error
	LoadPolicy() error
	Policy() map[string][]string
	PolicyName(name string) ([]string, bool)
	Transport() Transport
}

type manager struct {
	policy       map[string][]string
	actions      []Action
	groups       []Group
	actionsGroup []ActionGroup
	groupToGroup []GroupToGroup
	transport    Transport
}

func (m *manager) SavePolicy() error {
	return m.Transport().SavePolicy()
}

func (m *manager) LoadPolicy() error {
	return m.Transport().LoadPolicy()
}

func NewManager(tr Transport) Manager {
	return &manager{
		transport: tr,
	}
}

func (m *manager) Transport() Transport {
	return m.transport
}

func (m *manager) PolicyName(name string) ([]string, bool) {
	rule, found := m.Policy()[name]
	if found {
		sort.Strings(rule)
	}
	return rule, found
}

func (m *manager) Policy() map[string][]string {
	return m.policy
}

func (m *manager) AddGroup(g string) error {
	gr := Group{
		Name: g,
	}
	for _, cgr := range m.groups {
		if cgr == gr {
			return ErrGroupExist
		}
	}
	m.groups = append(m.groups, gr)
	return nil
}

func (m *manager) AddAction(a string) error {
	na := Action{
		Name: a,
	}
	for _, ca := range m.actions {
		if ca == na {
			return ErrActionExist
		}
	}
	m.actions = append(m.actions, na)
	return nil
}

func (m *manager) AddGroupToAction(g string, a string) error {
	nga := ActionGroup{
		Action: Action{Name: a},
		Group:  Group{Name: g},
	}
	//TODO если нет действия ошибку давать

	for _, cag := range m.actionsGroup {
		if cag == nga {
			return ErrGroupToActionExist
		}
	}
	m.actionsGroup = append(m.actionsGroup, nga)
	return nil
}

func (m *manager) AddGroupToGroup(g string, pg string) error {
	//Проверить нет ли рекурсивности наследования и если есть то кинуть ошибку
	//возьмем текущее состояние и проверим нет ли там уже самой себяшки
	_gtg := m.groupToGroups()
	parentExistGroup := func(g string, p string) bool {
		if _, eg := _gtg[g]; !eg {
			return false
		}
		for _, egg := range _gtg[g] {
			if egg == p {
				return true
			}
		}
		return false
	}
	if parentExistGroup(g, pg) {
		return ErrGroupToGroupRecursively
	}

	ngtg := GroupToGroup{
		Group:  Group{Name: g},
		Parent: Group{Name: pg},
	}

	//TODO проверка наличия группы источника и группы назначения иначе ошибка

	for _, cgtg := range m.groupToGroup {
		if cgtg == ngtg {
			return ErrGroupToGroupExist
		}
	}
	m.groupToGroup = append(m.groupToGroup, ngtg)
	return nil
}

func (m *manager) groupToGroups() map[string][]string {
	_gtg := make(map[string][]string)
	parentExistGroup := func(g string, p string) bool {
		if _, eg := _gtg[g]; !eg {
			return false
		}
		for _, egg := range _gtg[g] {
			if egg == p {
				return true
			}
		}
		return false
	}

	var loadGtg func(dst string, src string)
	loadGtg = func(dst string, src string) {
		var compare string
		if src == "" {
			compare = dst
			_gtg[dst] = append(_gtg[dst], dst)
		} else {
			compare = src
			_gtg[dst] = append(_gtg[dst], src)
		}

		for _, gtgCurrent := range m.groupToGroup {
			if parentExistGroup(dst, gtgCurrent.Group.Name) {
				continue
			}

			if gtgCurrent.Parent.Name == compare {
				loadGtg(dst, gtgCurrent.Group.Name)
			}
		}
	}
	for _, g := range m.groups {
		loadGtg(g.Name, "")
	}
	return _gtg
}

func (m *manager) RefreshPolicy() {
	_p := make(map[string][]string)

	_gtg := m.groupToGroups()
	//Пройти все группы
	var existActionGroup = func(action string, group string) bool {
		if _, ea := _p[action]; !ea {
			return false
		}
		for _, egr := range _p[action] {
			if egr == group {
				return true
			}
		}
		return false
	}

	for _, a := range m.actionsGroup {
		//Если есть уже такое правило и у правила есть роль то скип
		ea := existActionGroup(a.Action.Name, a.Group.Name)
		if ea {
			continue
		}
		eag := existActionGroup(a.Action.Name, a.Group.Name)
		if eag {
			continue
		}
		_p[a.Action.Name] = append(_p[a.Action.Name], a.Group.Name)
		////если у роли для действия есть последователи они тоже могут
		children, foundChildren := _gtg[a.Group.Name]
		if !foundChildren {
			continue
		}
		for _, child := range children {
			eagc := existActionGroup(a.Action.Name, child)
			if eagc {
				continue
			}
			_p[a.Action.Name] = append(_p[a.Action.Name], child)
		}
	}
	m.policy = _p
}
